from functions import m_json
from functions import m_pck

#Versuch Wärmekapazität:

path = ""
#Metadaten laden
metadata = m_json.get_metadata_from_setup("/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json")
#Sensor serials zu Metadaten hinzufügen
metadata = m_json.add_temperature_sensor_serials("/home/pi/calorimetry_home/datasheets",metadata)
#Messdaten erfassen
data = m_pck.get_meas_data_calorimetry(metadata)
#Messdaten in Hdf5 datei speichern
m_pck.logging_calorimetry(data, metadata,"/home/pi/calorimetry_home/data/data_heat_capacity","/home/pi/calorimetry_home/datasheets")
m_json.archiv_json("/home/pi/calorimetry_home/datasheets","/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json","/home/pi/calorimetry_home/data/data_heat_capacity")

print("Zweiten Versuch vorbereiten:")

#Versuch Newtonsches Abkühlungsgesetz:

#Metadaten laden
metadata = m_json.get_metadata_from_setup("/home/pi/calorimetry_home/datasheets/setup_newton.json")
#Sensor serials zu Metadaten hinzufügen
metadata = m_json.add_temperature_sensor_serials("/home/pi/calorimetry_home/datasheets",metadata)
#Messdaten erfassen
data = m_pck.get_meas_data_calorimetry(metadata)
#Messdaten in Hdf5 datei speichern
m_pck.logging_calorimetry(data, metadata,"/home/pi/calorimetry_home/data/data_newton","/home/pi/calorimetry_home/datasheets")
m_json.archiv_json("/home/pi/calorimetry_home/datasheets","/home/pi/calorimetry_home/datasheets/setup_newton.json", "/home/pi/calorimetry_home/data/data_newton")


